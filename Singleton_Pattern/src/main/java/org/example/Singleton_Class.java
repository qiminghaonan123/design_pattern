package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Singleton_Class {

    List<String> good = Arrays.asList(new String[]{"1", "2", "3", "4", "5", "6"});
    int i=0;
    private Singleton_Class(){}
    private static Singleton_Class singletonClass=new Singleton_Class();

    public static synchronized  Singleton_Class getSingletonClass() {
        return singletonClass;
    }
    public void singletinSay(){
        System.out.println("这是单例模式");
    }

    public void getGood(){
        System.out.println(good.get(i));
        good.remove(i++);
    }
}
