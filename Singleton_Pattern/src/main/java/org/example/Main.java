package org.example;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(new Runnable() {
                public void run() {
                    Singleton_Class singletonClass = Singleton_Class.getSingletonClass();
                    singletonClass.getGood();
                }
            }).start();
        }
        Singleton.NEWSINGLETON.singletonSay();

    }
}