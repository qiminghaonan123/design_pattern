package org.example;

public class Cook {
    public void makeFriedRice() {
        System.out.println("厨师正在做炒饭。");
    }

    public void makeNoodles() {
        System.out.println("厨师正在做面条。");
    }
}
