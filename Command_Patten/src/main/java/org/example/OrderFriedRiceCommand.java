package org.example;

public class OrderFriedRiceCommand implements Command{

    private Cook cook;

    public OrderFriedRiceCommand(Cook cook) {
        this.cook = cook;
    }

    @Override
    public void execute() {
        cook.makeFriedRice();
    }
}
