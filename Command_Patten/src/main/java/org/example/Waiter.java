package org.example;

public class Waiter {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void placeOrder() {
        if (command!= null) {
            command.execute();
        }
    }
}
