package org.example;

public class Main {
    public static void main(String[] args) {
        Cook cook = new Cook();
        OrderFriedRiceCommand orderFriedRiceCommand = new OrderFriedRiceCommand(cook);
        OrderNoodlesCommand orderNoodlesCommand = new OrderNoodlesCommand(cook);
        Waiter waiter = new Waiter();
        waiter.setCommand(orderNoodlesCommand);
        waiter.placeOrder();
        waiter.setCommand(orderFriedRiceCommand);
        waiter.placeOrder();
    }
}