package org.example;

public class OrderNoodlesCommand implements Command{
    private  Cook cook;

    public OrderNoodlesCommand(Cook cook) {
        this.cook = cook;
    }

    @Override
    public void execute() {
        cook.makeNoodles();
    }
}
