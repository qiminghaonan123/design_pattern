package org.example;

import java.util.ArrayList;
import java.util.List;

public class Meal {
    private List<Item> items = new ArrayList<Item>();
    public void addItem(Item item) {
        items.add(item);
    }
    public float getCost() {
        float cost = 0.0f;
        for (Item item : items) {
            cost += item.price();
        }
        return cost;
    }
    public void showItems() {
        for (Item item : items) {
            System.out.print("商品有 : " + item.name());
            System.out.print(", 使用包装 : " + item.packing().pack());
            System.out.println(", 价格 : " + item.price());
        }
    }
}
