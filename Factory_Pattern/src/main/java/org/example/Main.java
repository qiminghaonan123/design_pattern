package org.example;

public class Main {
    public static void main(String[] args) {
        Factory factory = new Factory();
        Shape circle = factory.setShape("Circle");
        Shape rectangle = factory.setShape("Rectangle");
        Shape triangle = factory.setShape("Triangle");
        circle.draw();
        rectangle.draw();
        triangle.draw();
    }
}