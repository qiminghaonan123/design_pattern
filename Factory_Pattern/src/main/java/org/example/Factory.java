package org.example;

public class Factory {
    public Shape setShape(String shape) {
        if (shape.equals("Circle")) {
            return new Circle();
        }
        if (shape.equals("Rectangle")) {
            return new Rectangle();
        }
        if (shape.equals("Triangle")) {
            return new Rectangle();
        }
        return null;
    }
}
